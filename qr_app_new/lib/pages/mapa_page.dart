import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qr_app_new/models/scan_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapaPage extends StatefulWidget {
  //const MapaPage({Key key}) : super(key: key);

  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  Completer<GoogleMapController> _controller = Completer();
  MapType mapType = MapType.normal;  // tipo de mapa que se muestra en este caso inicia con normal

  @override
  Widget build(BuildContext context) {


    final ScanModel scan = ModalRoute.of(context).settings.arguments;


    final CameraPosition puntoInicial = CameraPosition(
      target: scan.getLatLng(),
      zoom: 18.5,
      tilt: 50
    );

    Set<Marker> markers = new Set<Marker>();
    markers.add(
      Marker(
        markerId: MarkerId('Punto Scaneado'),
        position: scan.getLatLng(),
        //icon: BitmapDescriptor.defaultMarkerWithHue(220.0)
        )
    );
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Mapa'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.gps_fixed),
            onPressed: ()async{
              final GoogleMapController controller = await _controller.future;
              controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
                target: scan.getLatLng(),
                zoom: 18.5,
                tilt: 50
                )));
            },
          )
        ],
      ),
      body: GoogleMap(
        //myLocationButtonEnabled: true,
        //myLocationEnabled: true,
        markers: markers,
        mapType: mapType, 
        //mapToolbarEnabled: false,
        zoomControlsEnabled: false, /// quita la barra de ayudea de zoom + - 
        //zoomGesturesEnabled: true,// permite activar los gestos de zoom con dos dedos. psdta: por default siempre está en true
        initialCameraPosition: puntoInicial,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.tune),
        onPressed: () { 
         if(mapType==MapType.normal){
           mapType=MapType.terrain;
           print('terrain');
         }else{
           if(mapType==MapType.terrain){
              mapType=MapType.satellite;
              print('satelite');
           }else{
             if (mapType==MapType.satellite) {
               mapType=MapType.hybrid;
               print('Hybrid');
             } else {
               mapType=MapType.normal;
               print('volvio a normal');
             }
           }
         }

         setState(() { });
         },

      ),
    );
  }
}
