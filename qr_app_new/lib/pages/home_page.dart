import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_app_new/pages/direcciones_list_page.dart';
import 'package:qr_app_new/pages/mapas_list_page.dart';
import 'package:qr_app_new/providers/db_provider.dart';
import 'package:qr_app_new/providers/scan_list_provider.dart';
import 'package:qr_app_new/providers/ui_provider.dart';
import 'package:qr_app_new/widgets/custom_navigatorbar.dart';
import 'package:qr_app_new/widgets/scan_button.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historial'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              Provider.of<ScanListProvider>(context, listen: false).borrarAllScans();
            },
          )
        ],
      ),
      body: _HomePageBody(),
      bottomNavigationBar: CustomNavigationBar(),
      floatingActionButton: ScanButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}


class _HomePageBody extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

   /// Obtener el UiProvider para saber el Selected menu Opt
    
   final uiProvider = Provider.of<UiProvider>(context);

    final currenteIndex = uiProvider.selectedMenuOpt;

    //Usar el ScanListProvider
    final scanListProvider = Provider.of<ScanListProvider>(context, listen: false);

    switch ( currenteIndex) {
      case 0 : 
      scanListProvider.cargarScanPorTipo('geo');
      return MapasListPage();

      case 1 :
      scanListProvider.cargarScanPorTipo('http');
      return DireccionesListPage();
        
        
      default:
      return MapasListPage();
    }
  }
}