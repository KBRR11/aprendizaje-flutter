import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_app_new/providers/scan_list_provider.dart';
import 'package:qr_app_new/utils/utils.dart';

class ScanTiles extends StatelessWidget {
  final String tipo;

  const ScanTiles({Key key, this.tipo});

  @override
  Widget build(BuildContext context) {
    final scanListProvider = Provider.of<ScanListProvider>(context);
    
     
    return ListView.builder(
      itemCount: scanListProvider.scans.length,
      itemBuilder: (BuildContext context, int index) => Dismissible(
              key: UniqueKey(),
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: FractionalOffset(0.0,1.0) , 
                    end: FractionalOffset(0.23,1.0),
                    colors: [
                      Colors.red,
                      Theme.of(context).scaffoldBackgroundColor
                    ]
                  )
                ),
              ),
              onDismissed: (DismissDirection direction){
                Provider.of<ScanListProvider>(context, listen: false).borrarScanPorId(scanListProvider.scans[index].id);
              },
              child: ListTile(
          leading: Icon(
            this.tipo == 'http'
              ? Icons.http
              : Icons.map,
          color: Colors.teal,),
          title: Text(scanListProvider.scans[index].valor),
          subtitle: Text(scanListProvider.scans[index].id.toString()),
          trailing: Icon(
            this.tipo == 'http'
              ? Icons.arrow_forward
              : Icons.zoom_out_map,
          color: Colors.teal,) ,
          onTap: ()=> launchURL(context, scanListProvider.scans[index]),
        ),
      ),
      

    );
  }
}