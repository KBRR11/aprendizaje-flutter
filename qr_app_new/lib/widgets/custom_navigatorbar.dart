import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_app_new/providers/ui_provider.dart';

class CustomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
final uiProvider = Provider.of<UiProvider>(context);
    final currentIndex = uiProvider.selectedMenuOpt;

    return BottomNavigationBar(
      currentIndex: currentIndex,
      elevation: 0,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Mapas'),
        BottomNavigationBarItem(icon: Icon(Icons.http), label: 'Direcciones')
      ],
      onTap: (int i) => uiProvider.selectedMenuOpt=i,
    );
  }
}
