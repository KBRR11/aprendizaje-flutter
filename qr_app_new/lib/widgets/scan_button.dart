import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:qr_app_new/providers/scan_list_provider.dart';
import 'package:qr_app_new/utils/utils.dart';

class ScanButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 0,
      child: Icon(Icons.qr_code_scanner_outlined),
      onPressed: () async {
        String barcodeScanRes;
        // Platform messages may fail, so we use a try/catch PlatformException.

        final scanListProvider =
            Provider.of<ScanListProvider>(context, listen: false);

        try {
          if (barcodeScanRes == '-1') {
            return;
          }
          barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
              "#ec9c3d", "Cancelar", false, ScanMode.QR);
          //print(barcodeScanRes);
          final nuevoScan = await scanListProvider.nuevoScan(barcodeScanRes);
          launchURL(context, nuevoScan);
        } on PlatformException {
          barcodeScanRes = 'Ha ocurrido un error, Intentelo más tarde.';
        }
      },
    );
  }
}
