import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_app_new/pages/home_page.dart';
import 'package:qr_app_new/pages/mapa_page.dart';
import 'package:qr_app_new/providers/scan_list_provider.dart';
import 'package:qr_app_new/providers/ui_provider.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(

     providers: [
       ChangeNotifierProvider(create: (_)=> new UiProvider() ),
       ChangeNotifierProvider(create: (_)=> new ScanListProvider() ),
     ],

          child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'QR Reader',
        initialRoute: 'home',
        routes: {
          'home' : ( _ ) => HomePage(),
          'mapa' : ( _ ) => MapaPage(),
        },
        theme: ThemeData.dark()/*.copyWith(
          primaryColor: Colors.yellow[700]
        ),*/
      ),
    );
  }
}