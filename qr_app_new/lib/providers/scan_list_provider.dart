

import 'package:flutter/widgets.dart';
import 'package:qr_app_new/providers/db_provider.dart';

class ScanListProvider extends ChangeNotifier{

  List<ScanModel> scans = [];
  String tipoSeleccionado = 'http';

  Future<ScanModel> nuevoScan(String valor) async{
    final nuevoScan = new ScanModel(valor: valor);
    final id = await DBProvider.db.nuevoScan(nuevoScan);
    // Asignar el ID de la Base de datos al modelo
    nuevoScan.id = id;

    if( this.tipoSeleccionado == nuevoScan.tipo){
      this.scans.add(nuevoScan);
      notifyListeners();
    }
    return nuevoScan;
  }

cargarScans() async{
 final scans = await DBProvider.db.getAllScans();
 this.scans = [...scans];
 notifyListeners();
}

  cargarScanPorTipo( String tipo ) async{
    final scans = await DBProvider.db.getScansTipo(tipo);
 this.scans = [...scans];
 this.tipoSeleccionado = tipo;
 notifyListeners();
  }

borrarAllScans() async{
  await DBProvider.db.deleteAllScans();
  this.scans = [];
  notifyListeners();
}

borrarScanPorId( int id ) async{
  await DBProvider.db.deleteScanById(id);
  this.cargarScanPorTipo( this.tipoSeleccionado );
  
}


}