import 'package:flutter/material.dart';
import 'package:qr_scanner_app/src/pages/direcciones_page.dart';
import 'package:qr_scanner_app/src/pages/mapas_page.dart';
import 'package:barcode_scan/barcode_scan.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR - Scanner', style: TextStyle(color: Theme.of(context).primaryColor),),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever , color: Theme.of(context).primaryColor,), 
            onPressed: (){}
            )
        ],
        backgroundColor: Color.fromRGBO(37, 40, 47, 1.0),
      ),
      body: _cargarPage(currentIndex),
      backgroundColor:Color.fromRGBO(69, 74, 82, 1.0),
      bottomNavigationBar: _crearBottomNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.qr_code_scanner),
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: _scanQR
      ),
    );
  }

  _scanQR() async{

    //https://github.com/KBRR11
    //http://maps.google.com/maps?q=51.4170326,5.4451063
    // geo:51.4170326,5.4451063

    dynamic futureString = '';

    try {
      futureString = await BarcodeScanner.scan();
    }catch(e){
      futureString=e.toString();
    }
 
  print('Future String: ${futureString.rawContent}');

  if (futureString != null) {
      print('tenemos informacion');
    }
  }

  _cargarPage(int paginaActual) {
    switch (paginaActual) {
      case 0:
        return MapasPage();
      case 1:
        return DireccionesPage();

      default:
        return MapasPage();
    }
  }

  _crearBottomNavigationBar() {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(37, 40, 47, 1.0),
        textTheme: Theme.of(context).textTheme.copyWith(
          caption: TextStyle(color: Color.fromRGBO(181, 181, 190, 1.0))
        )
      ),
          child: BottomNavigationBar(
        
          currentIndex: currentIndex,
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.map), title: Text('Mapa')),
            BottomNavigationBarItem(
                icon: Icon(Icons.http_sharp), title: Text('Direcciones')),
          ]),
    );
  }
}
