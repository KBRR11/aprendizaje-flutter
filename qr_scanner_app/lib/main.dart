import 'package:flutter/material.dart';
import 'package:qr_scanner_app/src/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'QR-Scan Pro',
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context) => HomePage()
      },
      theme: ThemeData(
        primaryColor: Color.fromRGBO(219, 169, 23, 1.0),
        //textTheme: Theme.of(context).textTheme.apply(bodyColor: Colors.white)
      ),
    );
  }
}