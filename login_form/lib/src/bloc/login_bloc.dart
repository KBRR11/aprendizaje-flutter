import 'dart:async';

import 'package:login_form/src/bloc/validators.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators {
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  // Recuperar datos del Stream
  Stream<String> get emailStream =>
      _emailController.stream.transform(validarEmail);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);


// Rxdart para convinar respuestas y en base a eso permitir acciones o no
  Stream<bool> get formValidStream =>
      Rx.combineLatest2(emailStream, passwordStream, (e, p) => true);

  //Insertar valores al Stream
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  //Obtener utimos valores de los Streams
  String get obtenerEmail => _emailController.value;
  String get obtenerPass => _passwordController.value;

  dispose() {
    _emailController?.close();
    _passwordController?.close();
  }
}
