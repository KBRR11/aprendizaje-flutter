import 'package:flutter/material.dart';
//import 'package:login_form/src/bloc/provider.dart';
import 'package:login_form/src/models/producto_model.dart';

import 'package:login_form/src/providers/producto_provider.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  
  @override
  _HomePageState createState() => _HomePageState();
  
}

class _HomePageState extends State<HomePage> {
  final productoProvider = new ProductosProvider();



  @override
  Widget build(BuildContext context) {
    //final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('HOME PAGE'),
      ),
      body: _crearListado(),
      floatingActionButton: _crarProductoBtn(context),
    );
  }

  Widget _crearListado() {
     return FutureBuilder(
       future: productoProvider.cargarPorductos(), 
       builder: (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot) {
         if (snapshot.hasData) {
           final productos = snapshot.data;
           return ListView.builder(
             itemCount: productos.length,
             itemBuilder: (context, i ) => _crearItem( context ,productos[i]),
           );
         } else {
           return Center(child: CircularProgressIndicator(),);
         }
         },
       
     );
  }

  Widget _crearItem(BuildContext context, ProductoModel producto) {
    return Dismissible(
         key: UniqueKey(),
         background: Container(
           color: Colors.red,
         ),
         onDismissed: (direccion){
           productoProvider.borrarProducto(producto.id);
           Timer(Duration(seconds: 1), () {
           _crearListado();
           setState(() {
             
           });

           } 
           );
         },
          child: Card(
            child: Column(
              children: [
                SizedBox(height: 10.0,),
                (producto.fotoUrl == null)
                ? Image(image: AssetImage('assets/no-image.png'),)
                : FadeInImage(
                  image: NetworkImage(producto.fotoUrl), 
                  placeholder: AssetImage('assets/jar-loading.gif'),
                  height: 300.0,
                  width: double.infinity,
                  fit: BoxFit.cover,
                  ),
                  ListTile(
                   title: Text('${ producto.titulo } - ${ producto.valor }'),
                   subtitle: Text('${ producto.id }'),
                   onTap: () => Navigator.pushNamed(context, 'producto' ,arguments: producto).then((value) {
                    setState(() { });
                   }),
              ),
              ],
            ),
          )
    );

    
  }

  Widget _crarProductoBtn(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: Colors.deepPurple,
      onPressed: () {
        Navigator.pushNamed(context, 'producto').then((value)  {
          setState(() {
             
           });
        });
      },
    );
  }
}
