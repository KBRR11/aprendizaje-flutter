import 'dart:io';

import 'package:flutter/material.dart';
import 'package:login_form/src/models/producto_model.dart';

import 'package:login_form/src/providers/producto_provider.dart';
import 'package:login_form/src/utils/utils.dart' as utils;
import 'package:image_picker/image_picker.dart';
import 'dart:async';

class ProductoPage extends StatefulWidget {
  //const ProductoPage({Key key}) : super(key: key);

  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final productoProvider = new ProductosProvider();

  ProductoModel producto = new ProductoModel();
  bool _guardando = false;
  File _image;
  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) {
      producto = prodData;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Nuevo Producto'),
        actions: [
          IconButton(
            icon: Icon(Icons.photo_size_select_actual_outlined),
            onPressed: _seleccionarFoto,
          ),
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _tomarFoto,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                _mostrarFoto(),
                _crearNombre(),
                _crearPrecio(),
                _crearDisponible(),
                _crearBoton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: producto.titulo,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto',
      ),
      onSaved: (value) => producto.titulo = value,
      validator: (value) {
        if (value.length < 3) {
          return 'ingrese el nombre del Producto';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearPrecio() {
    return TextFormField(
      initialValue: producto.valor.toString(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Precio',
      ),
      onSaved: (value) => producto.valor = double.parse(value),
      validator: (value) {
        if (utils.isNumeric(value)) {
          return null;
        } else {
          return 'Sólo Números';
        }
      },
    );
  }

  Widget _crearDisponible() {
    return SwitchListTile(
      value: producto.disponible,
      title: Text('Disponible'),
      activeColor: Colors.deepPurple,
      onChanged: (bool value) => setState(() {
        producto.disponible = value;
      }),
    );
  }

  _crearBoton() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.deepPurple,
      textColor: Colors.white,
      icon: Icon(Icons.save),
      label: Text('Guardar'),
      onPressed: (_guardando) ? null : _submit,
    );
  }

  void _submit() async{
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();
    // cuando el formulario es válido

    setState(() {
      _guardando = true;
    });

    if( _image!=null ){
     producto.fotoUrl = await productoProvider.uploadImage(_image);
    }

    if (producto.id == null) {
      productoProvider.crearProducto(producto);
      setState(() {
        _guardando = false;
      });
      mostrarSnackBar('${producto.titulo}, fue creado con éxito!');
    } else {
      productoProvider.editarProducto(producto);
      setState(() {
        _guardando = false;
      });
      mostrarSnackBar('${producto.titulo}, fue actualizado con éxito!');
    }
    Timer(Duration(seconds: 1), () => Navigator.pop(context));
  }

  void mostrarSnackBar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _mostrarFoto() {
 
    if (producto.fotoUrl != null) {
 
      return FadeInImage(
        image: NetworkImage(producto.fotoUrl),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        fit: BoxFit.contain,
      );
 
    } else {
 
      if( _image != null ){
        return Image.file(
          _image,
          fit: BoxFit.cover,
          height: 300.0,
        );
      }
      return Image.asset('assets/no-image.png');
    }
  }

  Future _seleccionarFoto() async {
    _procesarSolicitudImagen(ImageSource.gallery);
  }

  Future _tomarFoto() async{
    _procesarSolicitudImagen(ImageSource.camera);
  }

  Future _procesarSolicitudImagen(ImageSource origen) async{
     final pickedFile = await picker.getImage(source: origen);

    setState(() {
      if (pickedFile != null) {
        producto.fotoUrl = null;
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
}
