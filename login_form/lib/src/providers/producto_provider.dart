import 'dart:convert';
import 'dart:io';


import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:login_form/src/models/producto_model.dart';
import 'package:login_form/src/user_preferences/preferencias_user.dart';
import 'package:mime_type/mime_type.dart';


class ProductosProvider {
  final String _url =
      'https://flutter-projects-e1985-default-rtdb.firebaseio.com';

  final _prefs = new PreferenciasUsuario();    

  Future<bool> crearProducto(ProductoModel producto) async {
    final url = '$_url/productos.json?auth=${ _prefs.token }';

    final resp = await http.post(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);

    return true;
  }

  Future<bool> editarProducto(ProductoModel producto) async {
    final url = '$_url/productos/${ producto.id }.json?auth=${ _prefs.token }';

    final resp = await http.put(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);

    return true;
  }

  Future<List<ProductoModel>> cargarPorductos() async {
    final url = '$_url/productos.json?auth=${ _prefs.token }';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<ProductoModel> productos = new List();
    if (decodedData == null) return [];
    //print(decodedData);
   decodedData.forEach((id, prod) { 
     print(id);
      
      final prodTemp = ProductoModel.fromJson(prod);
      prodTemp.id = id;

      productos.add(prodTemp);

   });

    return productos;
  }

  Future<int> borrarProducto(String id) async{
    final url = '$_url/productos/$id.json?auth=${ _prefs.token }';
    final resp = await http.delete(url);

    print(resp.body);
    return 1;
  }

  Future<String> uploadImage(File image) async{
    final url = Uri.parse('https://api.cloudinary.com/v1_1/dee9edshv/image/upload?upload_preset=wkptlfpz');
    final mimeType = mime(image.path).split('/'); //image/jpg lo separamos del / 

    final imageUploadRequest = http.MultipartRequest(
      'POST',
      url
    );

    final file = await http.MultipartFile.fromPath(
      'file', 
      image.path,
      contentType: MediaType( mimeType[0], mimeType[1] ) // pide el tipo y subtipo en este caso: tipo imagen subtipo jpg
      );

      imageUploadRequest.files.add(file);

      final streamResponse = await imageUploadRequest.send();
      final resp = await http.Response.fromStream(streamResponse);

      if( resp.statusCode!= 200 && resp.statusCode !=201){
        print('Algo salio Mal!');
        print(resp.body);
        return null;
      }

      final respData = json.decode(resp.body);

      print(respData['secure_url']);

      return respData['secure_url'];

  }
}
