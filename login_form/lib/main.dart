import 'package:flutter/material.dart';
import 'package:login_form/src/bloc/provider.dart';

import 'package:login_form/src/pages/home_page.dart';
import 'package:login_form/src/pages/login_page.dart';
import 'package:login_form/src/pages/producto_page.dart';
import 'package:login_form/src/pages/registro_page.dart';
import 'package:login_form/src/user_preferences/preferencias_user.dart';
 
void main() async{
  
    
   runApp(MyApp());

   final prefs = new PreferenciasUsuario();
    await prefs.initPrefs();
    print(prefs.token);
} 
 
class MyApp extends StatelessWidget {
  
  
  @override
  Widget build(BuildContext context) {
    //final prefs = new PreferenciasUsuario();
    //print(prefs.token);
    return Provider(
      child:MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login Auth',
      initialRoute: 'login',
      routes: {
        'login'   : (BuildContext context) => LoginPage(),
        'registro': (BuildContext context) => RegistroPage(),
        'home'    : (BuildContext context) => HomePage(),
        'producto': (BuildContext context) => ProductoPage(),
      },
      theme: ThemeData(
        primaryColor: Colors.deepPurple[400]
      ),
    )
    );
    
    
    
    
  }
}