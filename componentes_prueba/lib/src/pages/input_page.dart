import 'package:flutter/material.dart';
import 'package:flutter/src/material/pickers/date_picker_dialog.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _password = '';
  String _fecha = '';
  String _opcionSelecionada = 'volar';
List<String> _poderes = ['volar', 'Super Velocidad', 'Super Aliento', 'Rayos X', 'Super Fuerza'];


TextEditingController _inputFieldDateController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Inputs"),
        centerTitle: true,
        backgroundColor: Colors.red[300],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearInputEmail(),
          Divider(),
          _crearInputPassword(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _crearDropDown(),
          Divider(),
          _mostrarDatosPersona(),
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        counter: Text("Letras ${_nombre.length}"),
        hintText: "Nombre de la persona",
        labelText: 'Nombre',
        helperText: 'solo es el primer nombre',
        suffixIcon: Icon(Icons.person),
        icon: Icon(Icons.account_circle),
      ),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  Widget _crearInputEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        hintText: "Email",
        labelText: 'Correo Electrónico',
        suffixIcon: Icon(Icons.alternate_email),
        icon: Icon(Icons.email),
      ),
      onChanged: (valor) {
        setState(() {
          _email = valor;
        });
      },
    );
  }

  Widget _crearInputPassword() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        hintText: "Password",
        labelText: 'Password',
        suffixIcon: Icon(Icons.lock_open),
        icon: Icon(Icons.lock),
      ),
      onChanged: (valor) {
        setState(() {
          _password = valor;
        });
      },
    );
  }

  Widget _mostrarDatosPersona() {
    return ListTile(
      title: Text("El nombre es: $_nombre"),
      subtitle: Text("El Email es: $_email \nPassword: $_password"),
      leading: Icon(Icons.verified_user),
      trailing: Text(_opcionSelecionada),
    );
  }

  Widget _crearFecha(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: "Fecha de Nacimiento",
        labelText: 'Fecha de Nacimiento',
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectdate(context);
      },
    );
  }

  _selectdate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        helpText: 'Seleccionar Fecha',
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1990),
        lastDate: new DateTime(2025),
        initialDatePickerMode: DatePickerMode.year,
        locale: Locale('es','ES')
        );

        if(picked!=null){
          setState(() {
            _fecha = picked.toString();
            _inputFieldDateController.text = _fecha;
          });
        }
  }

List<DropdownMenuItem<String>> getOpcionesDropDown() {
  List<DropdownMenuItem<String>> lista = new List();
  _poderes.forEach((poder) { 
          lista.add(DropdownMenuItem(
            child: Text(poder),
            value: poder,
            ));
        });
  return lista;

}

  Widget _crearDropDown() {
    return Row(
      children:<Widget> [
      Icon(Icons.bubble_chart),
      SizedBox(width: 30.0),
        DropdownButton(
          value: _opcionSelecionada,
          items: getOpcionesDropDown(),
          onChanged: (opt){
            setState(() {
              _opcionSelecionada = opt;
            });

          }
           ),
      ],
    );
  }
}
