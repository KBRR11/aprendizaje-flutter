import 'dart:async';

import 'package:flutter/material.dart';

class ListViewPage extends StatefulWidget {
  

  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {

ScrollController _scrollController = new ScrollController();

  List<int> _listaNumeros = new List();
int _ultimoItem=0;
bool _isLoading = false;

@override
  void initState() {
      super.initState();
      _agregar10();

      _scrollController.addListener(() { 
        if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
         // _agregar10();
          fetchData();
        }
      });
  }

  @override
  void dispose() {
    super.dispose();

    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text("ListView Builder"),
         centerTitle: true,
         backgroundColor: Colors.deepPurple[300],
       ),
       body: Stack(
         children: <Widget> [
           _crearLista(),
           _crearLoading(),
         ],
       ),
    );
  }

  Widget _crearLista() {
    return RefreshIndicator(
      onRefresh: obtenerNuevasImagenes,
          child: ListView.builder(
        controller: _scrollController,
        itemCount: _listaNumeros.length,
        itemBuilder: ( BuildContext context, int index){
          final imagen = _listaNumeros[index];
return 
FadeInImage(
placeholder: AssetImage('assets/Curve-Loading.gif'), 
image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'));
        }
        ),
    );
  }

  void _agregar10(){
    for (var i = 1; i <= 10; i++) {
      _ultimoItem++;
      _listaNumeros.add(_ultimoItem);
    }

    setState(() {
      
    });
  }

  Future fetchData() async{
_isLoading = true;

setState(() {});

final duration = Duration(seconds: 2);
return new Timer(duration, respuestaHTTP);

  }

 void respuestaHTTP(){
   _isLoading = false;
   _agregar10();

   _scrollController.animateTo(
     _scrollController.position.pixels+100, 
     duration: Duration(milliseconds: 250), 
     curve: Curves.fastOutSlowIn);
 }

 Widget _crearLoading(){
   if (_isLoading) {
     return Column(
       mainAxisSize: MainAxisSize.max,
       mainAxisAlignment: MainAxisAlignment.end,
       children: <Widget>[
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget> [
          //LinearProgressIndicator()
          CircularProgressIndicator()
           ],
         ),
         SizedBox(height: 15.0,)
       ],
       
     );
   }else{
     return Container();
   }
 }

 Future obtenerNuevasImagenes() async{
   final duration = new Duration(seconds: 2);
   new Timer(duration, (){
_listaNumeros.clear();
_ultimoItem++;
_agregar10();
   });
   return Future.delayed(duration);
 }
}