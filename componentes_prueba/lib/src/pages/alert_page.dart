import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alertas"),
        centerTitle: true,
      ),
      body:Center(
        child: RaisedButton(
          child: Text("Mostrar Alerta"),
          color: Colors.blue[600],
          textColor: Colors.white,
          shape: StadiumBorder(),
          onPressed: (){
            _mostrarAlerta(context);
                      },
                      
                      )
                  ),
                  floatingActionButton: FloatingActionButton(
                    child: Icon(Icons.keyboard_return),
                    onPressed: (){
                      Navigator.pop(context);
                    }
                    ),
                    floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
                );
                 
              }
            
              void _mostrarAlerta( BuildContext context) {
showDialog(
  
  context: context,
  barrierDismissible: true,
  builder: (context){
    return AlertDialog(
      title: Text("Título de Alerta"),
      content: Column(
        mainAxisSize: MainAxisSize.min, /// para que no se  haga demasiado grande y solo ocupe lo que hay de contenido
        children: [
          Image(image: AssetImage("assets/404pngGift.gif")),
          Text("Alerta No Encontrada"),
        ],
      ),
      actions: <Widget>[
        FlatButton( 
          child: Text("Cancelar"),
          textColor: Colors.red
        ,onPressed: ()=> Navigator.of(context).pop()
        ),
        FlatButton( 
          child: Text("Ok"),
          //textColor: Colors.red,
        onPressed: (){
          Navigator.of(context).pop();
        }
        )
      ],
    );
  }
  );

              }
}