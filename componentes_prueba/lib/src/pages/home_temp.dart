import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = [
    "opción 1",
    "opción 2",
    "opción 3",
    "opción 4",
    "opción 5",
    "opción 6",
    "opción 7",
    "opción 8",
    "opción 9",
    "opción 10"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Prueba de Componentes FLUTTER"),
        centerTitle: true,
      ),
      body: //ListView(children: _crearItems()),
      ListView(children:_crearItemFcorta())
    );
  }

  /*List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    for (var opc in opciones) {
      final tempWidget = ListTile(
        title: Text(opc),
      );
      lista
        ..add(tempWidget)
        ..add(
          Divider(
            color: Colors.orange[700],
          ),
        );
    }
    return lista;
  }*/

  List<Widget> _crearItemFcorta() {
    return opciones.map((itemOpc) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(itemOpc),
            subtitle: Text("Forma corta"),
            leading: Icon(Icons.album, color: Colors.deepPurple),
            trailing: Icon(Icons.keyboard_arrow_down, color: Colors.orange[700],),
            onTap: (){},
          ),
          Divider(color: Colors.lightBlue[900],)
        ],
      );
    }).toList();
  }
}
