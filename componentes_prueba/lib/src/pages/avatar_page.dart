import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
 
 static final pagename = 'avatar';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Avatars"),
       // centerTitle: true,
       actions: [
         Container(
           padding: EdgeInsets.all(5.0),
           child: CircleAvatar(
             radius: 23.0,
             backgroundImage: NetworkImage("https://scontent.fgye15-1.fna.fbcdn.net/v/t1.0-9/71496918_2900621060002100_7768635788719292416_o.jpg?_nc_cat=100&_nc_sid=09cbfe&_nc_eui2=AeFpT43AeovW1tunAq3SkOFoPMFawf5awPg8wVrB_lrA-OxD18I1YodpBTaer4Zyrl6O1RtCxD-jokziVI3FNOKw&_nc_ohc=1Mwa0TpoC7gAX-vDmV6&_nc_ht=scontent.fgye15-1.fna&oh=b1b486aa2730461b214f1d41ea8326a6&oe=5F99FFE5")
             ),
             
         ),
         
         Container(
           margin: EdgeInsets.only(right: 10.0 ),
           child: CircleAvatar(
             child: Text("KR"), 
             
             backgroundColor: Colors.brown,
             foregroundColor: Colors.white,
             
           ),
         ),
       ],
      ),
      body: Center(
        child: FadeInImage(placeholder: AssetImage("assets/Curve-Loading.gif"), image: NetworkImage("https://scontent.fgye15-1.fna.fbcdn.net/v/t1.0-9/118436799_3740026912728173_4409704140916397662_o.jpg?_nc_cat=109&_nc_sid=730e14&_nc_eui2=AeH556h4q4-l2YXopxCKE5nOt0Z42qEL6XO3RnjaoQvpc4u1KL9tyzjkESbuPjDjAz195FK7D_EC3MvPZH5yLNZt&_nc_ohc=-MKxGNPlDicAX-UHHMI&_nc_ht=scontent.fgye15-1.fna&oh=7c50bd97b16f40b4a02cf0ed19fdd6c1&oe=5F982EB4")
        , fadeInDuration: Duration(milliseconds: 200),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_return),
        onPressed: (){
          Navigator.pop(context);
        }
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
     
  }
}