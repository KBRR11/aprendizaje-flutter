import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Card Page"),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo1(),
         SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(
              Icons.add_shopping_cart,
              color: Colors.blue[300],
            ),
            title: Text("Hola soy el Titulo de este Card"),
            subtitle: Text(
                "Esta es la descripción de este Card que se debe mostrar."),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(onPressed: () {}, child: Text("Cancelar")),
              FlatButton(onPressed: () {}, child: Text("Ok"))
            ],
          )
        ],
      ),
    );

    
  }

  Widget _cardTipo2() {
    final card = Container(
      
      child: Column(
        children: <Widget>[
         FadeInImage(placeholder: AssetImage('assets/blue-loader.gif'), image: NetworkImage(
                  'https://www.xtrafondos.com/wallpapers/diseno-de-paisaje-del-espacio-5771.jpg'),
                  fadeInDuration: Duration(milliseconds: 200),
                  height: 300.0,
                  fit: BoxFit.cover,
                  ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget> [
               ListTile(
                 title: Text("Titulo del LandScape"),
                 subtitle: Text("Descripción del LandScape."),
               )
              ],
            ),
          )
        ],
      ),
    );
    return Container(
      
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
BoxShadow(
  color: Colors.black26,
  blurRadius: 10.0,
  spreadRadius: 2.0,
  offset: Offset(2.0,10.0),
)
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
}

//// PONER LUEGO SI QUIERES
/*floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_return),
        onPressed: (){
          Navigator.pop(context);
        }
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,*/
