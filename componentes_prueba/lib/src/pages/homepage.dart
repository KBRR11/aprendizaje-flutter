//import 'package:componentes_prueba/src/pages/alert_page.dart';
import 'package:componentes_prueba/src/providers/menu_provider.dart';
import 'package:componentes_prueba/src/utils/icon_string_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Componentes Flutter"),
        centerTitle: true,
      ),
      body: _lista(),
    );
    
  }

 Widget _lista() {
   
   return FutureBuilder(
     future: menuProvider.cargarData(),
     initialData: [],
     builder: (context, AsyncSnapshot<List<dynamic>> snapshot){

return ListView(
  children: _listaItems( snapshot.data, context),
);

     },
    );


 }

  List<Widget> _listaItems(List<dynamic> data , BuildContext context) {
final List<Widget> opciones = [];

data.forEach((opt) { 
  final widgetTemp = ListTile(
title: Text(opt['texto']),
leading: getIcon(opt['icon']),
trailing:Icon(Icons.keyboard_arrow_right, color: Colors.blue[700],) ,
onTap: () {

Navigator.pushNamed(context, opt['ruta']);

  ///FORMA ESTATICA DE NAVEGAR A UNA RUTA
   /*final route = MaterialPageRoute(
     builder: (context) => AlertPage()
   );

Navigator.push(context, route);*/

},
  );

opciones..add(widgetTemp)
        ..add(Divider(color: Colors.blue[900],));

});
return opciones;
  }
}
