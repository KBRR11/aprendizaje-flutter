import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

double _valorSlider = 100.0;
bool _bloquearCheck =false;

class _SliderPageState extends State<SliderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Slider',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: Container(
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: <Widget>[
              _crearSlider(),
              _crearCheckBox(),
              _crearSwitch(),
              Expanded(child: _crearImagen())
            ],
          )),
    );
  }

  Widget _crearSlider() {
    return Slider(
        activeColor: Colors.amber,
        /*divisions: 20,
      label: 'Tamaño de Ant-man',*/ //LA Divivision y el Label siempre van de la Mano, Label no funciona sin division.
        value: _valorSlider,
        min: 10.0,
        max: 400.0,
        onChanged:(_bloquearCheck)?null: (valor) {
          setState(() {
            _valorSlider = valor;
          });
        });
  }

  Widget _crearCheckBox() {
    return CheckboxListTile(
      title: Text("Bloquear Slider:"),
      value: _bloquearCheck, 
      onChanged: (valor){
setState(() {
  _bloquearCheck = valor;
});
      }
      );
  }

_crearSwitch() {
  return SwitchListTile(
    title: Text("Bloquear Slider:"),
      value: _bloquearCheck, 
      onChanged: (valor){
setState(() {
  _bloquearCheck = valor;
});
      }
  );
}

  Image _crearImagen() {
    return Image(
      image: NetworkImage(
          'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/5b8d2b12-21e8-4931-8a6d-fb9ecdd60383/dcc5qj5-6d473056-8bbf-488d-b285-ef9f9803722a.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvNWI4ZDJiMTItMjFlOC00OTMxLThhNmQtZmI5ZWNkZDYwMzgzXC9kY2M1cWo1LTZkNDczMDU2LThiYmYtNDg4ZC1iMjg1LWVmOWY5ODAzNzIyYS5wbmcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.NjH9PWtUwnhQDSDR7aq0pxi3najiQxKwZOxTikb3VWQ'),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }

  
}
