import 'package:componentes_prueba/src/pages/listview_page.dart';
import 'package:flutter/material.dart';
import 'package:componentes_prueba/src/pages/alert_page.dart';
import 'package:componentes_prueba/src/pages/avatar_page.dart';
import 'package:componentes_prueba/src/pages/homepage.dart';
import 'package:componentes_prueba/src/pages/card_page.dart';
import 'package:componentes_prueba/src/pages/animated_container.dart';
import 'package:componentes_prueba/src/pages/input_page.dart';
import 'package:componentes_prueba/src/pages/slider_page.dart';


Map  <String, WidgetBuilder> getAplicationRoutes(){
return
<String, WidgetBuilder>{
        '/' : (BuildContext context) => HomePage(), 
        'alert' : (BuildContext context) => AlertPage(), 
        AvatarPage.pagename : (BuildContext context) => AvatarPage(), 
        'card' : (BuildContext context) => CardPage(), 
        'animatedContainer': (BuildContext context) => AnimatedContainerPage(),
        'inputs':(BuildContext context) => InputPage(),
        'slider':(BuildContext context) =>SliderPage(),
        'listview':(BuildContext context) =>ListViewPage(),
      };
}
