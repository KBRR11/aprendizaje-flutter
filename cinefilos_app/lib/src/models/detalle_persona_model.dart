class DetallesPersonas {
  List<DetallePersona> detallesPersonas = new List();

DetallesPersonas();

DetallesPersonas.fromJsonList( List<dynamic> jsonList ) {
print(jsonList);
if(jsonList == null) return;

for (var item in jsonList) {
  final detallePersona = new DetallePersona.fromJsonMap(item);
  detallesPersonas.add(detallePersona);
}

}
}

class DetallePersona {
  bool adult;
 // List<String> alsoKnownAs;
  String biography;
  String birthday;
  dynamic deathday;
  int gender;
  String homepage;
  int id;
  String imdbId;
  String name;
  String placeOfBirth;
  double popularity;
  String profilePath;

  DetallePersona({
    this.adult,
    //this.alsoKnownAs,
    this.biography,
    this.birthday,
    this.deathday,
    this.gender,
    this.homepage,
    this.id,
    this.imdbId,
    this.name,
    this.placeOfBirth,
    this.popularity,
    this.profilePath,
  });

  DetallePersona.fromJsonMap(Map<String, dynamic> json){
    adult = json['adult'];
   // alsoKnownAs = json['also_known_as'];
    biography = json['biography'];
    birthday = json['birthday'];
    deathday = json['deathday'];
    gender = json['gender'];
    homepage = json['homepage'];
    id = json['id'];
    imdbId = json['imdb_id'];
    name = json['name'];
    placeOfBirth = json['place_of_birth'];
    popularity = json['popularity'];
    profilePath = json['profile_path'];

  }

 

}
