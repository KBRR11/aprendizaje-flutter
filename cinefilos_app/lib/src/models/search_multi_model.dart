

class SearchResultados{
  
  List<SearchResult> resultados = new List();

//SearchResultados();

SearchResultados.fromJsonList(List<dynamic> jsonList) {

//print(jsonList);
    if (jsonList.length == 0) return;
    

    jsonList.forEach((item) { 
      final busqueda = new SearchResult.fromJsonMap(item);
      resultados.add(busqueda);
      //print(busqueda.knownForDepartment);
    });
    

  }
}



class SearchResult {
    String originalName;
    int id;
    String mediaType;
    String name;
    double popularity;
    int voteCount;
    double voteAverage;
    String firstAirDate;
    String posterPath;
    List<int> genreIds;
    String originalLanguage;
    String backdropPath;
    String overview;
    //List<dynamic> originCountry;
    bool video;
    String title;
    String releaseDate;
    String originalTitle;
    bool adult;
    String knownForDepartment;
    List<KnownFor> knownFor;
    String profilePath;
    int gender;

  SearchResult({
    this.originalName,
    this.id,
    this.mediaType,
    this.name,
    this.popularity,
    this.voteCount,
    this.voteAverage,
    this.firstAirDate,
    this.posterPath,
    this.genreIds,
    this.originalLanguage,
    this.backdropPath,
    this.overview,
    //this.originCountry,
    this.video,
    this.title,
    this.releaseDate,
    this.originalTitle,
    this.adult,
    this.knownForDepartment,
    this.knownFor,
    this.profilePath,
    this.gender,
  });

  SearchResult.fromJsonMap(Map<String, dynamic> json) {
    originalName       = json['original_name']; 
    id                 = json['id'];
    mediaType          = json['media_type'];
    name               = json['name'];
    popularity         = json['popularity']/1;
    voteCount          = json['vote_count'];
    voteAverage        = json['vote_average']/1;
    firstAirDate       = json['first_air_date'];
    posterPath         = json['poster_path'];
    genreIds           : json["genre_ids"] == null ? null : List<int>.from(json["genre_ids"].map((x) => x));
    originalLanguage   = json['original_language'];
    backdropPath       = json['backdrop_path'];
    overview           = json['overview'];
    //originCountry      = json['origin_country'];
    video              = json['video'];
    title              = json['title'];
    releaseDate        = json['release_date'];
    originalTitle      = json['original_title'];
    adult              = json['adult'];
    knownForDepartment = json['known_for_department'];
    knownFor           : knownFor == null ? null : List<dynamic>.from(knownFor.map((x) => x.toJson()));
    profilePath        = json['profile_path'];
    gender             = json['gender'];
  }

 Map<String, dynamic> toJson() => {
        "original_name": originalName == null ? null : originalName,
        "id": id,
        "media_type": mediaType == null ? null : mediaType,
        "name": name == null ? null : name,
        "popularity": popularity,
        "vote_count": voteCount == null ? null : voteCount,
        "vote_average": voteAverage == null ? null : voteAverage,
        "first_air_date": firstAirDate == null ? null : firstAirDate,
        "poster_path": posterPath == null ? null : posterPath,
        "genre_ids": genreIds == null ? null : List<dynamic>.from(genreIds.map((x) => x)),
        //"original_language": originalLanguage == null ? null : originalLanguageValues.reverse[originalLanguage],
        "backdrop_path": backdropPath == null ? null : backdropPath,
        "overview": overview == null ? null : overview,
        //"origin_country": originCountry == null ? null : List<dynamic>.from(originCountry.map((x) => x)),
        "video": video == null ? null : video,
        "title": title == null ? null : title,
        "release_date": releaseDate == null ? null : releaseDate,
        "original_title": originalTitle == null ? null : originalTitle,
        "adult": adult == null ? null : adult,
        "known_for_department": knownForDepartment == null ? null : knownForDepartment,
        "known_for": knownFor == null ? null : List<dynamic>.from(knownFor.map((x) => x.toJson())),
        "profile_path": profilePath == null ? null : profilePath,
        "gender": gender == null ? null : gender,
    };

  getImgResult() {
    if (posterPath == null && profilePath == null) {
      return 'https://www.sinergiamarketingturistico.com/wp-content/uploads/2016/02/no-photo-available.png';
    } else {
     if(profilePath == null){
      return 'https://image.tmdb.org/t/p/w500/$posterPath';
     }else{
       return 'https://image.tmdb.org/t/p/w500/$profilePath';
     }
    }
  }
 
  getNameOrTitle() {
    if (title == null) {
      return name;
    } else {
      return title;
    }
  }
}


class KnownFors{
  List<KnownFor> listadoKnown = new List();

//SearchResultados();

KnownFors.fromJsonList(List<dynamic> jsonList) {
//print(jsonList);
    if (jsonList.length == 0) return;
    
    jsonList.forEach((item) { 
      final resultLista = new KnownFor.fromJsonMap(item);
      listadoKnown.add(resultLista);
      //print(busqueda);
    });
    

  }
}

class KnownFor {
    KnownFor({
        this.posterPath,
        this.voteCount,
        this.video,
        this.mediaType,
        this.id,
        this.adult,
        this.backdropPath,
        this.originalLanguage,
        this.originalTitle,
        this.genreIds,
        this.title,
        this.voteAverage,
        this.overview,
        this.releaseDate,
    });

    String posterPath;
    int voteCount;
    bool video;
    String mediaType;
    int id;
    bool adult;
    String backdropPath;
    String originalLanguage;
    String originalTitle;
    List<int> genreIds;
    String title;
    double voteAverage;
    String overview;
    String releaseDate;

    factory KnownFor.fromJsonMap(Map<String, dynamic> json) => KnownFor(
        posterPath: json["poster_path"],
        voteCount: json["vote_count"],
        video: json["video"],
        mediaType: json["media_type"],
        id: json["id"],
        adult: json["adult"],
        backdropPath: json["backdrop_path"],
        originalLanguage: json["original_language"],
        originalTitle: json["original_title"],
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        title: json["title"],
        voteAverage: json["vote_average"].toDouble(),
        overview: json["overview"],
        releaseDate: json["release_date"],
    );

    Map<String, dynamic> toJson() => {
        "poster_path": posterPath,
        "vote_count": voteCount,
        "video": video,
        "media_type": mediaType,
        "id": id,
        "adult": adult,
        "backdrop_path": backdropPath,
        "original_language": originalLanguage,
        "original_title": originalTitle,
        "genre_ids": List<dynamic>.from(genreIds.map((x) => x)),
        "title": title,
        "vote_average": voteAverage,
        "overview": overview,
        "release_date": releaseDate,
    };
}

