class Peliculas {

List<Pelicula> items = new List();

Peliculas();

Peliculas.fromJsonList( List<dynamic> jsonList ) {

if(jsonList == null) return;

for (var item in jsonList) {
  final pelicula = new Pelicula.fromJsonMap(item);
  items.add(pelicula);
}

} 


}


class Pelicula {
  String uniqueId;  ////// creamos Ids unicos para Hero Animation
  double popularity;
  double voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String title;
  double voteAverage;
  String overview;
  String releaseDate;

  Pelicula({
    this.popularity,
    this.voteCount,
    this.video,
    this.posterPath,
    this.id,
    this.adult,
    this.backdropPath,
    this.originalLanguage,
    this.originalTitle,
    this.genreIds,
    this.title,
    this.voteAverage,
    this.overview,
    this.releaseDate,
  });

  Pelicula.fromJsonMap( Map<String, dynamic> json){
     popularity       = json['popularity'] /1; 
     voteCount        = json['vote_count'] / 1;
     video            = json['video'];
     posterPath       = json['poster_path'];
     id               = json['id'];
     adult            = json['adult'];
     backdropPath     = json['backdrop_path'];
     originalLanguage = json['original_language'];
     originalTitle    = json['original_title'];
     genreIds         = json['genre_ids'].cast<int>();
     title            = json['title'];
     voteAverage      = json['vote_average'] / 1;
     overview         = json['overview'];
     releaseDate      = json['release_date'];
   
    
  }

  getPosterImg(){
    if(posterPath == null){
      return 'https://www.sinergiamarketingturistico.com/wp-content/uploads/2016/02/no-photo-available.png';
    }else{
      return 'https://image.tmdb.org/t/p/w500/$posterPath';
    }
  }

  getPosterBackground(){
if(backdropPath == null){
      return 'https://www.sinergiamarketingturistico.com/wp-content/uploads/2016/02/no-photo-available.png';
    }else{
      return 'https://image.tmdb.org/t/p/w500/$backdropPath';
    }
  }

  getSimilarPoster() {
    if (posterPath == null) {
      return 'https://medicality.es/wp-content/uploads/2019/05/Aibr5LR8T.jpg';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$posterPath';
    }
  }
}
