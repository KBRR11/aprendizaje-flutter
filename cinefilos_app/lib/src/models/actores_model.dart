class Cast {
  // Para manejar  una lista de  actores

  List<Actor> actores = new List();

  Cast.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    jsonList.forEach((item) {
      final actor = Actor.fromJsonMap(item);
      actores.add(actor);
    });
  }
}

class Actor {
  ///// para manejar un actor independiente
  //String uniqueIdHero;
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  Actor.fromJsonMap(Map<String, dynamic> json) {
    castId = json['cast_id'];
    character = json['character'];
    creditId = json['credit_id'];
    gender = json['gender'];
    id = json['id'];
    name = json['name'];
    order = json['order'];
    profilePath = json['profile_path'];
  }

  getProfileImg() {
    if (profilePath == null) {
      return 'https://static8.depositphotos.com/1207999/1027/i/450/depositphotos_10274329-stock-photo-man-office-avatar-blue.jpg';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$profilePath';
    }
  }
}

class Produccion{
List<Crew> equipo = new List();

  Produccion.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    jsonList.forEach((item) {
      final integrante = Crew.fromJsonMap(item);
      equipo.add(integrante);
    });
  }
}



class Crew {   ///// reto personal para hacer
  String creditId;
  String department;
  int gender;
  int id;
  String job;
  String name;
  String profilePath;

  Crew({
    this.creditId,
    this.department,
    this.gender,
    this.id,
    this.job,
    this.name,
    this.profilePath,
  });

  Crew.fromJsonMap(Map<String, dynamic> json){
    
    creditId = json['credit_id'];
    department = json['department'];
    gender = json['gender'];
    id = json['id'];
    job = json['job'];
    name = json['name'];
    profilePath = json['profile_path'];
  }


  getProfileImgCrew() {
    if (profilePath == null) {
      return 'https://static8.depositphotos.com/1207999/1027/i/450/depositphotos_10274329-stock-photo-man-office-avatar-blue.jpg';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$profilePath';
    }
  }


}
