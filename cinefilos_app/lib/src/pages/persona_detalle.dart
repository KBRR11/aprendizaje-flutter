import 'package:cinefilos_app/src/models/actores_model.dart';
import 'package:cinefilos_app/src/models/detalle_persona_model.dart';
import 'package:cinefilos_app/src/providers/peliculas_provider.dart';
import 'package:flutter/material.dart';

class PersonaDetalle extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    final Actor actor = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppBar(actor),
          SliverList(
              delegate: SliverChildListDelegate([
            SizedBox(height: 10.0),
            _ponerTituloPersona(context, actor),
            SizedBox(
              height: 10.0,
            ),
            _buscarDatosBiography(actor.id),
          ]))
        ],
      ),
    );
  }

  Widget _crearAppBar(Actor actor) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 70.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          actor.name,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
      ),
    );
  }

  Widget _ponerTituloPersona(BuildContext context, Actor actor) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 20.0),
          //color: Colors.amber,
          child: Row(
            children: <Widget>[
              Hero(
                tag: actor.creditId,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image(
                    image: NetworkImage(actor.getProfileImg()),
                    height: 190.0,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Flexible(child: _buscarDatosPersona(actor.id))
            ],
          ),
        ),
      ],
    );
  }

  Widget _buscarDatosPersona(int id) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
        future: peliProvider.getDetallePersona(id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _mostrarDetalle(snapshot.data);
          } else {
            //print('else $snapshot.data');
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _buscarDatosBiography(int id) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
        future: peliProvider.getDetallePersona(id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _mostrarBiography(snapshot.data);
          } else {
            //print('else $snapshot.data');
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _mostrarDetalle(DetallePersona detalle) {
    
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text('Nacimiento: ', style: TextStyle(fontWeight: FontWeight.bold)),
            Text(detalle.birthday ?? 'Desconocido',
                style: TextStyle(fontStyle: FontStyle.italic))
          ],
        ),
        Row(
          children: [
            Text('Lugar: ', style: TextStyle(fontWeight: FontWeight.bold)),
            Container(
                width: 170.0,
                child: Text(
                  detalle.placeOfBirth ?? 'Desconocido',
                  style: TextStyle(fontStyle: FontStyle.italic),
                  overflow: TextOverflow.ellipsis,
                ))
          ],
        ),
        Row(
          children: [
            Text('Muerte: ', style: TextStyle(fontWeight: FontWeight.bold)),
            Text(detalle.deathday ?? 'Desconocido',
                style: TextStyle(fontStyle: FontStyle.italic))
          ],
        ),
        Row(
          children: [
            Text('Tipo: ', style: TextStyle(fontWeight: FontWeight.bold)),
            if (detalle.gender == 1)
              Text('Actriz', style: TextStyle(fontStyle: FontStyle.italic)),
            if (detalle.gender == 2)
              Text('Actor', style: TextStyle(fontStyle: FontStyle.italic))
          ],
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          children: [
            Text('Popularidad: ',
                style: TextStyle(fontWeight: FontWeight.bold)),
            Icon(
              Icons.star_half_rounded,
              color: Colors.yellow[900],
            ),
            Text(detalle.popularity.toString() ?? 'Desconocido',
                style: TextStyle(fontStyle: FontStyle.italic))
          ],
        ),
      ],
    );
  }

  Widget _mostrarBiography( DetallePersona detalle) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Biografía:', style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(
            height: 10.0,
          ),
          Text(detalle.biography ,textAlign: TextAlign.justify,),
          if(detalle.biography == '')
Text('Lo sentimos, no tenemos datos sobre este personaje...',style: TextStyle(fontStyle: FontStyle.italic),)
        ],
      ),
    );
  }
}
