

import 'package:flutter/material.dart';
import 'package:cinefilos_app/src/models/actores_model.dart';
import 'package:cinefilos_app/src/models/pelicula_model.dart';
import 'package:cinefilos_app/src/providers/peliculas_provider.dart';
//import 'package:flutter/gestures.dart';


class PeliculaDetalle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;
   

    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        _crearAppBar(pelicula),
        SliverList(
          delegate: SliverChildListDelegate([
            SizedBox(height: 10.0),
            _posterTitulo(context, pelicula),
            _descripcion(pelicula),
            Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "Reparto:",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                )
              ],
            ),
            _crearCasting(pelicula),
             Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "Producción:",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                )
              ],
            ),
            _crearCrew(pelicula),
            Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "Películas Similares:",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                )
              ],
            ),
            _crearSimilares(pelicula)
          ]),
        )
      ],
    ));
  }

  Widget _crearAppBar(Pelicula pelicula) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          pelicula.title,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          placeholder: AssetImage('assets/img/loading.gif'),
          image: NetworkImage(pelicula.getPosterBackground()),
          fadeInDuration: Duration(milliseconds: 150),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _posterTitulo(BuildContext context, Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Hero(
            tag: pelicula.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(pelicula.getPosterImg()),
                height: 150.0,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Flexible(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(pelicula.title,
                  style: Theme.of(context).textTheme.headline6,
                  overflow: TextOverflow.ellipsis),
              Text(
                pelicula.originalTitle,
                style: Theme.of(context).textTheme.caption,
                overflow: TextOverflow.ellipsis,
              ),
              Row(
                children: [
                  Icon(
                    Icons.star_half,
                    color: Colors.yellow[800],
                  ),
                  Text(pelicula.voteAverage.toString())
                ],
              )
            ],
          ))
        ],
      ),
    );
  }

  Widget _descripcion(Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        pelicula.overview,
        textAlign: TextAlign.justify,
      ),
    );
  }

  Widget _crearCasting(Pelicula pelicula) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
      future: peliProvider.getCast(pelicula.id.toString()),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return _crearActoresPageView(snapshot.data, pelicula);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _crearActoresPageView(List<Actor> actores, Pelicula pelicula) {
    return SizedBox(
      height: 245.0,
      child: PageView.builder(
        pageSnapping: false,
        itemCount: actores.length,
        controller: PageController(viewportFraction: 0.36, initialPage: 1),
        itemBuilder: (context, i) {
          return _actorTarjeta(context, actores[i], pelicula);
        },
        
      ),
    );
  }

  Widget _actorTarjeta(BuildContext context, Actor actor, Pelicula pelicula) {
    //actor.uniqueIdHero='${ actor.uniqueIdHero }-${ pelicula.title }';
    
    final actorTarjeta = Container(
      //color: Colors.cyan,
      child: Column(
        children: <Widget>[
          Hero(
            tag: actor.creditId ,
                      child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  image: NetworkImage(actor.getProfileImg()),
                  height: 200.0,
                  //width: 200.0,
                  fit: BoxFit.cover,
                )),
          ),
          Text(
            actor.name,
            style: Theme.of(context).textTheme.subtitle1,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            actor.character,
            style: Theme.of(context).textTheme.caption,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
    return GestureDetector(
      child: actorTarjeta,
      onTap: (){
        Navigator.pushNamed(context, 'persona', arguments: actor);
      },
    );
  }

  Widget _crearCrew(Pelicula pelicula) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
      future: peliProvider.getCrew(pelicula.id.toString()),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return _crearProduccionPageView(snapshot.data);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _crearProduccionPageView(List<Crew> equipo) {
    return SizedBox(
      height: 245.0,
      child: PageView.builder(
        pageSnapping: false,
        itemCount: equipo.length,
        controller: PageController(viewportFraction: 0.36, initialPage: 1),
        itemBuilder: (context, i) {
          return _prodTarjeta(context, equipo[i]);
        },
      ),
    );
  }

  Widget _prodTarjeta(BuildContext context, Crew integrante) {
    return Container(
      //color: Colors.cyan,
      child: Column(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/no-image.jpg'),
                image: NetworkImage(integrante.getProfileImgCrew()),
                height: 200.0,
                //width: 200.0,
                fit: BoxFit.cover,
              )),
          Text(
            integrante.name,
            style: Theme.of(context).textTheme.subtitle1,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            integrante.job,
            style: Theme.of(context).textTheme.caption,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }

  Widget _crearSimilares(Pelicula pelicula) {
    final peliProvider = new PeliculasProvider();
    return FutureBuilder(
      future: peliProvider.getSimilars(pelicula.id.toString()),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return _crearSimilarsPageView(snapshot.data);
        } else {
          
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _crearSimilarsPageView(List<Pelicula> similars) {
    return SizedBox(
      height: 245.0,
      child: PageView.builder(
        pageSnapping: false,
        itemCount: similars.length,
        controller: PageController(viewportFraction: 0.36, initialPage: 1),
        itemBuilder: (context, i) {
          return _similarsTarjeta(context, similars[i]);
        },
      ),
    );
  }

  Widget _similarsTarjeta(BuildContext context, Pelicula similarMovie) {

    similarMovie.uniqueId = '${ similarMovie.id }-similarMovieHRZTL';
    final tarjetaSimilar = Container(
      //color: Colors.cyan,
      child: Column(
        children: <Widget>[
          Hero(
            tag: similarMovie.uniqueId ,
                      child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  image: NetworkImage(similarMovie.getSimilarPoster()),
                  height: 200.0,
                  //width: 200.0,
                  fit: BoxFit.cover,
                )),
          ),
          Text(
            similarMovie.title,
            style: Theme.of(context).textTheme.subtitle1,
            overflow: TextOverflow.ellipsis,
          ),

          Row(
            children: [
              Icon(Icons.star_border_rounded , color: Colors.yellow[800],),
              Text(
                similarMovie.popularity.toString(),
                style: Theme.of(context).textTheme.caption,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ],
      ),
    );

    return GestureDetector(
      child: tarjetaSimilar,
      onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: similarMovie);
      },
    );
  }
}
