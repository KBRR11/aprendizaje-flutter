import 'package:cinefilos_app/src/search/search_delegate.dart';
import 'package:flutter/material.dart';
import 'package:cinefilos_app/src/providers/peliculas_provider.dart';
import 'package:cinefilos_app/src/widgets/card-swiper_widget.dart';
import 'package:cinefilos_app/src/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {

peliculasProvider.getPopulares();
peliculasProvider.getValorados();
peliculasProvider.getUpcoming();

    return Scaffold(
      appBar: AppBar(
        title: Text("Cinéfilos App"),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {
           showSearch(context: context, delegate: DataSearch()); 
          })
        ],
      ),
      body: ListView(
        children: <Widget> [
          Column(  //////////////// POSIBLEMENTE QUITEMOS EL COLUMN
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _swiperTarjetas(),
            SizedBox(height: 10.0,),
            _populares(context),
            _valorados(context),
            _upcomings(context),
          ],
        ),
      ]),
    );
  }

  Widget _swiperTarjetas() {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return CardSwiper(peliculas: snapshot.data);
        } else {
          return Container(
            height: 400.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
    /* return */
  }

  Widget _populares(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start ,/// sirve para alinear el contenido de la Columna
        children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20.0),
          child: Text(
            "Populares",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(height: 10.0),

        StreamBuilder(
          stream: peliculasProvider.popularesStream,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              return MovieHorizontal(
                peliculas: snapshot.data,
                siguientePagina: peliculasProvider.getPopulares,
                tipo: 'populares',
                );
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ]),
    );
  }

  Widget _valorados(BuildContext context){
    return Container(
      
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start ,/// sirve para alinear el contenido de la Columna
        children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20.0),
          child: Text(
            "Los más valorados",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(height: 10.0),
        StreamBuilder(
          stream: peliculasProvider.valoradosStream,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              return MovieHorizontal(
                peliculas: snapshot.data,
                siguientePagina: peliculasProvider.getValorados,
                tipo: 'valorados',
                );
            } else {
             return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ]),
    );
  }

Widget _upcomings(BuildContext context){
    return Container(
      
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start ,/// sirve para alinear el contenido de la Columna
        children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20.0),
          child: Text(
            "Próximamente",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(height: 10.0),
        StreamBuilder(
          stream: peliculasProvider.upcomingStream,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              return MovieHorizontal(
                peliculas: snapshot.data,
                siguientePagina: peliculasProvider.getUpcoming,
                tipo: 'upcomings',
                );
            } else {
             return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ]),
    );
  }
}
