
import 'package:flutter/material.dart';
import 'package:cinefilos_app/src/models/search_multi_model.dart';
import 'package:cinefilos_app/src/providers/peliculas_provider.dart';

class DataSearch extends SearchDelegate {
  String seleccion = '';
  final peliculasProvider = new PeliculasProvider();

  @override
  List<Widget> buildActions(BuildContext context) {
    // buildActions son las acciones de nuestro appBar, ejemplo: un icono que borra el contenido de busqueda
    return [
      IconButton(
          icon: Icon(Icons.clear_rounded),
          onPressed: () {
            query = '';
          }),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // buildLeading es el icono que ponemos en la izquierda del buscador
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // buildResults se encarga de mostrar los resulados de busqueda
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blue[200],
        child: Center(
          child: Text(seleccion),
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // buildSuggestions se encarga de dar sugerencias de busqueda cuando la persona escribe
    if (query.isEmpty) {
      return Container();
    }

    return FutureBuilder(
        future: peliculasProvider.searchQuery(query),
        builder:
            (BuildContext context, AsyncSnapshot<List<SearchResult>> snapshot) {
          if (snapshot.hasData) {
            final resultados = snapshot.data;
//print('HOLA ESTO ES: $snapshot.data ');
            return ListView(
                children: resultados.map((resultSearh) {
              return 
            
              ListTile(
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(3.0),
                                  child: FadeInImage(
                    placeholder: AssetImage('assets/img/no-image.jpg'), 
                    image: NetworkImage(resultSearh.getImgResult()),
                    width: 50.0,
                    fit: BoxFit.cover,
                    ),
                ),
                title: Text(resultSearh.getNameOrTitle() ,overflow: TextOverflow.ellipsis,),
                subtitle: Row(
                  children: [
                    
                    Icon(Icons.star_half_outlined , color: Colors.yellow[800],),
                    Text('${resultSearh.popularity}'),
                   
                    
                  ],
                ),
                trailing: Text(resultSearh.mediaType),
                onTap: (){},
              );
            }).toList());
          } else {
            print("ELSE $snapshot.data");
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
