

import 'package:cinefilos_app/src/models/pelicula_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CardSwiper extends StatelessWidget {
   
  final List <Pelicula> peliculas;

   CardSwiper({ @required this.peliculas });

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;


    return Container(
      padding: EdgeInsets.only(top: 10.0),
      height: 425.0,
      //margin: EdgeInsets.only(bottom: 200.0),
      //color: Colors.indigo,
      child: Swiper(
        
        itemWidth: _screenSize.width * 0.7,
        itemHeight:  _screenSize.height * 0.5,
        layout: SwiperLayout.STACK,
        itemBuilder: (BuildContext context, int index) {


          peliculas[index].uniqueId = '${ peliculas[index].id }-enCines';
          return Hero(
            tag: peliculas[index].uniqueId,
                      child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: GestureDetector(
                            child: FadeInImage(
                  image: NetworkImage(peliculas[index].getPosterImg()),
                  placeholder: AssetImage('assets/Curve-Loading.gif'),
                  fit:  BoxFit.cover,
                  ),
                  onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: peliculas[index]);
      },
              ),
                
            ),
          );
        },
        itemCount: peliculas.length,
        //pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ),
    );

  }
}