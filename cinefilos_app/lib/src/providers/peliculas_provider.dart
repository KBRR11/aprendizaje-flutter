import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:cinefilos_app/src/models/actores_model.dart';
import 'package:cinefilos_app/src/models/detalle_persona_model.dart';
import 'package:cinefilos_app/src/models/search_multi_model.dart';

import 'package:http/http.dart' as http;
import 'package:cinefilos_app/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = 'f3cb11408b33cee67371458a1544508e';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';
  String _languageUSA = 'en-US';
  

  bool _cargandoPopulares = false;
  bool _cargandoValorados = false;
  bool _cargandoUpcomings = false;

  int _popularesPage = 0;
  int _valoradosPage = 0;
  int _upcomingsPage = 0;

  List<Pelicula> _populares = new List();
  List<Pelicula> _valorados = new List();
  List<Pelicula> _upcoming = new List();
  List<Pelicula> _similares = new List();
 
  final _popularesStreamController =
      StreamController<List<Pelicula>>.broadcast();
  final _valoradosStreamController =
      StreamController<List<Pelicula>>.broadcast();
  final _upcomingStreamController =
      StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink =>
      _popularesStreamController.sink.add;
  Stream<List<Pelicula>> get popularesStream =>
      _popularesStreamController.stream;

  Function(List<Pelicula>) get valoradosSink =>
      _valoradosStreamController.sink.add;
  Stream<List<Pelicula>> get valoradosStream =>
      _valoradosStreamController.stream;

  Function(List<Pelicula>) get upcomingSink =>
      _upcomingStreamController.sink.add;
  Stream<List<Pelicula>> get upcomingStream => _upcomingStreamController.stream;

  void disposeStreams() {
    _popularesStreamController?.close();
    _valoradosStreamController?.close();
    _upcomingStreamController?.close();
  }

  Future<List<Pelicula>> procesarRespuesta(Uri url) async {
    final resp = await http.get(url);
    final dataDecoded = json.decode(resp.body);

    final peliculas = new Peliculas.fromJsonList(dataDecoded['results']);
//print( peliculas.items[0].title);

    return peliculas.items;
  }

  Future<List<Pelicula>> getEnCines() async {
    final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key': _apikey,
      'language': _language,
    });

    return await procesarRespuesta(url);
  }

  Future<List<Pelicula>> getPopulares() async {
    if (_cargandoPopulares) return [];

    _cargandoPopulares = true;

    _popularesPage++;
    final url = Uri.http(_url, '3/movie/popular', {
      'api_key': _apikey,
      'language': _language,
      'page': _popularesPage.toString()
    });

    final resp = await procesarRespuesta(url);
    _populares.addAll(resp);
    popularesSink(_populares);
    _cargandoPopulares = false;
    return resp;
  }

  Future<List<Pelicula>> getValorados() async {
    if (_cargandoValorados) return [];

    _cargandoValorados = true;

    _valoradosPage++;
    final url = Uri.http(_url, '3/movie/top_rated', {
      'api_key': _apikey,
      'language': _language,
      'page': _valoradosPage.toString()
    });

    final resp = await procesarRespuesta(url);
    _valorados.addAll(resp);
    valoradosSink(_valorados);
    _cargandoValorados = false;
    return resp;
  }

  Future<List<Pelicula>> getUpcoming() async {
    if (_cargandoUpcomings) return [];

    _cargandoUpcomings = true;

    _upcomingsPage++;
    final url = Uri.http(_url, '3/movie/upcoming', {
      'api_key': _apikey,
      'language': _language,
      'page': _upcomingsPage.toString()
    });

    final resp = await procesarRespuesta(url);
    _upcoming.addAll(resp);
    upcomingSink(_upcoming);
    _cargandoUpcomings = false;
    return resp;
  }

  Future<List<Actor>> getCast(String peliculaId) async {
    final url = Uri.http(_url, '3/movie/$peliculaId/credits', {
      'api_key': _apikey,
      'language': _language,
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    final cast = new Cast.fromJsonList(decodedData['cast']);
//print(cast.actores[1].creditId);
    return cast.actores;
  }

  Future<List<Crew>> getCrew(String peliculaId) async {
    final url = Uri.http(_url, '3/movie/$peliculaId/credits', {
      'api_key': _apikey,
      'language': _language,
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    final crew = new Produccion.fromJsonList(decodedData['crew']);

    return crew.equipo;
  }

  

  Future<List<Pelicula>> getSimilars(String peliculaId) async {
    final url = Uri.http(_url, '3/movie/$peliculaId/similar', {
      'api_key': _apikey,
      'language': _language,
    });

    final resp = await procesarRespuesta(url);
  _similares.addAll(resp);

    return resp;
  }

  Future<DetallePersona> getDetallePersona(int personaId) async {
    final url = Uri.http(_url, '3/person/$personaId', {
      'api_key': _apikey,
      'language': _language,
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    //print(decodedData);
    final detallesEs = new DetallePersona.fromJsonMap(decodedData);
    print(detallesEs.id);
    if (detallesEs.biography == '') {
      final urlUSA = Uri.http(_url, '3/person/$personaId', {
        'api_key': _apikey,
        'language': _languageUSA,
      });
      final respUSA = await http.get(urlUSA);
      final decodedDataUSA = json.decode(respUSA.body);
      final detallesUSA = new DetallePersona.fromJsonMap(decodedDataUSA);
      return detallesUSA;
    } else {
      
        return detallesEs;
      
    }
  }

  Future<List<SearchResult>> searchQuery(String query) async {
    final url = Uri.https(_url, '3/search/multi',
        {'api_key': _apikey, 'language': _language, 'query': query});
//print(query);
    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    //print(decodedData['results']); // llegan los datos
    final result = new SearchResultados.fromJsonList(decodedData['results']);
    //print(result.resultados[1].mediaType); /// vota null
    return result.resultados;
  }
}
