import 'package:cinefilos_app/src/pages/home_page.dart';
import 'package:cinefilos_app/src/pages/peliculas_detalle.dart';
import 'package:cinefilos_app/src/pages/persona_detalle.dart';
import 'package:flutter/material.dart';


 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cinéfilospedia',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/'      : (BuildContext context) => HomePage(),
        'detalle': (BuildContext context) => PeliculaDetalle(),
        'persona': (BuildContext context) => PersonaDetalle(),
      },
    );
  }
}