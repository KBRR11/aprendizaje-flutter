import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:ui';


class BotonesPage extends StatelessWidget {
  const BotonesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              children: [
                _titulos(),
                _botonesRedondeados()
                ],
            ),
          )
        ],
      ),
      bottomNavigationBar: _bottomNavigationBar(context) ,
    );
  }

  Widget _fondoApp() {
    final gradienteFondo = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset(0.0, 0.6),
              end: FractionalOffset(0.0, 1.0),
              colors: [
            Color.fromRGBO(52, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0)
          ])),
    );

    final cajaRosada = Transform.rotate(
        angle: -pi / 5.0,
        child: Container(
          height: 360.0,
          width: 360.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80.0),
              gradient: LinearGradient(colors: [
                Color.fromRGBO(236, 98, 188, 1.0),
                Color.fromRGBO(241, 142, 172, 1.0)
              ])),
        ));

    return Stack(
      children: [
        gradienteFondo,
        Positioned(top: -100, left: -20.0, child: cajaRosada)
      ],
    );
  }

  Widget _titulos() {
    return SafeArea(
      child: Container(
        //color: Colors.blue[200],
        width: 350.0,
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Classify transaction',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              'classify this transaction into a particular category',
              style: TextStyle(color: Colors.white, fontSize: 18.0),
              //textAlign: TextAlign.justify,
            )
          ],
        ),
      ),
    );
  }

  Widget _botonesRedondeados(){
return Table(
  children: [
    TableRow(
      children: [
        _crearBotonRedondeado(Icons.games, Colors.greenAccent, 'VideoJuegos'),
        _crearBotonRedondeado(Icons.qr_code, Colors.blue, 'Scan'),
      ]
    ),
    TableRow(
      children: [
        _crearBotonRedondeado(Icons.location_on, Colors.red[600], 'Encontrar'),
        _crearBotonRedondeado(Icons.car_rental, Colors.blueGrey[900], 'Pedir Uber'),
      ]
    ),
    TableRow(
      children: [
        _crearBotonRedondeado(Icons.local_offer, Colors.yellow[600], 'Ofertas en Ropa'),
        _crearBotonRedondeado(Icons.coronavirus_outlined, Colors.red, 'Recomendaciones'),
      ]
    ),
    TableRow(
      children: [
        _crearBotonRedondeado(Icons.room_service, Colors.orange, 'Servicio a la Habitación'),
        _crearBotonRedondeado(Icons.storefront_outlined, Colors.pinkAccent[400], 'Comprar'),
      ]
    ),
  ],
);
  }


  _crearBotonRedondeado(IconData icon, Color color, String texto) {
    return ClipRRect(
      clipBehavior: Clip.antiAliasWithSaveLayer,
          child: BackdropFilter(
        filter: ImageFilter.blur( sigmaX: 10.0, sigmaY: 10.0 ),
        child: Container(
          height: 180,
          margin: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Color.fromRGBO(62, 66, 107, 0.7),
            borderRadius: BorderRadius.circular(20.0)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(height: 5.0,),
              CircleAvatar(
                backgroundColor: color,
                radius: 35.0,
                child: Icon(icon, size: 30.0, color: Colors.white,),
              ),
              Text(texto, style: TextStyle(color: color),),
              SizedBox(height: 5.0,),
            ],
          ),
        ),
      ),
    );
  }

  Widget _bottomNavigationBar( BuildContext context ){
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
        primaryColor: /*Colors.pinkAccent,*/Color.fromRGBO(240, 134, 221, 1.0),
        textTheme: Theme.of(context).textTheme.copyWith(
          caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0))
        )
      ), 
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today_outlined),
            title: Container()
            ),
            BottomNavigationBarItem(
            icon: Icon(Icons.stacked_line_chart_sharp),
            title: Container()
            ),
            BottomNavigationBarItem(
            icon: Icon(Icons.person_pin),
            title: Container()
            )
        ]
        )
    );
  }

}
