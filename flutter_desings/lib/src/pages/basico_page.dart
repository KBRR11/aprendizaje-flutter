import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {
  final estiloTitle = TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0);
  final estiloSubtitle =
      TextStyle(fontWeight: FontWeight.w400, color: Colors.grey);
  final colorTitleButtons = TextStyle(color: Colors.blue, fontSize: 15.0);
  final styleParrafo = TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          _crearImagen(),

          //SizedBox(height: 20.0,),// sirve pero usaremos la propiedad padding de un container

          _crearTitle(),

          _crearBotones(),

          _crearTexto(),

          _crearTexto(),

          _crearTexto(),

          _crearTexto(),
        ],
      ),
    ));
  }

  Widget _crearImagen() {
    return Image(
        image: NetworkImage(
            'https://i.pinimg.com/originals/a1/78/55/a1785592d41e140f00ef1cf3d9597dcb.png'));
  }

  Widget _crearTitle() {
    return SafeArea(
          child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Atardecer en el Bosque ', style: estiloTitle),
                  SizedBox(
                    height: 08.0,
                  ),
                  Text('Ohio, USA', style: estiloSubtitle),
                ],
              ),
            ),
            Icon(Icons.star, color: Colors.yellow[900]),
            Text('41', style: TextStyle(fontSize: 17.0))
          ],
        ),
      ),
    );
  }

  Widget _crearBotones() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _accion(Icons.call, 'CALL'),
          _accion(Icons.near_me, 'ROUTE'),
          _accion(Icons.share_sharp, 'SHARE'),
        ],
      ),
    );
  }

  Widget _accion(IconData icon, String texto) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.blue,
          size: 35.0,
        ),
        SizedBox(
          height: 8.0,
        ),
        Text(texto, style: colorTitleButtons)
      ],
    );
  }

  Widget _crearTexto() {
    return SafeArea(
          child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
          child: Text(
            'Nostrud commodo aliqua officia velit magna aliqua. Amet pariatur anim reprehenderit exercitation ullamco ullamco magna Lorem elit consequat enim est ex incididunt. Aliqua veniam elit ullamco enim eu id ea. Consectetur deserunt minim id nostrud. Quis sint labore labore dolor Lorem cillum id excepteur fugiat. Tempor eu labore duis voluptate Lorem pariatur do eu magna nulla consequat et. Quis occaecat nostrud amet occaecat voluptate laborum ea veniam sunt tempor duis reprehenderit.',
            style: styleParrafo,
            textAlign: TextAlign.justify,
          )),
    );
  }
}
