

import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  //const ScrollPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: [
          _pagina1(),
          _pagina2(context),
        ],
      ),
    );
  }

  Widget _pagina1() {
    return Stack(
      children: [
        _colorFondo(),
        _imagenFondo(),
        _textos()
      ],
    );
  }

 Widget _colorFondo() {
   return Container(
     width: double.infinity,
     height: double.infinity,
     color: Color.fromRGBO(108,192,218,1.0),
   );
 }

 Widget _imagenFondo(){
   return Container(
     width: double.infinity,
     height: double.infinity,
     child: 
     Image(
       image: AssetImage('assets/scroll-1.png'),
       fit: BoxFit.cover,
       ),
   );
 }

 Widget _textos(){
   final estiloTexto = TextStyle(color: Colors.white, fontSize: 50.0);
   return SafeArea(
     child:Column(
      children: [
        SizedBox(height: 40.0,),
        Text('11°', style: estiloTexto,),
        SizedBox(height: 7.0,),
        Text("Miercoles", style: estiloTexto),
        Expanded(child: Container()),
        Icon(Icons.keyboard_arrow_down, size: 70.0, color: Colors.white,)
      ],
     ) 
     );
 }

  Widget _pagina2(BuildContext context) {
    return Stack(
      children: [
        _colorFondo(),
        _crearBotonWelcome(context)
      ],
    );
  }

  Widget _crearBotonWelcome(BuildContext context) {
    return Center(
      child: RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
        child: Text('Bienvenidos' , style:  TextStyle(fontSize: 20.0),) ,),
        onPressed: () {
          Navigator.pushNamed(context, 'botones');
        },
        shape: RoundedRectangleBorder(
  borderRadius: BorderRadius.circular(28.0),
  side: BorderSide(color: Colors.blue)
),
      ),
    );
  }

 
}
