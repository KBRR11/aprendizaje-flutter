import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_desings/src/pages/basico_page.dart';
import 'package:flutter_desings/src/pages/botones_page.dart';
import 'package:flutter_desings/src/pages/scroll_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

/*SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle.light.copyWith(
  statusBarColor: Colors.pink[300]
) );*/   //no lo uso porque me cambia el color del contenedor de notificaciones en telefonos Xiaomi
SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent
));

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Diseños',
     initialRoute: 'scroll',
     routes: {
      'basico' : (BuildContext context) => BasicoPage(),
      'scroll' : (BuildContext context) => ScrollPage(),
      'botones' : (BuildContext context) => BotonesPage(),    
     },
    );
  }
}