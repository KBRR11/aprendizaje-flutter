import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget{
@override
  State<StatefulWidget> createState() {
    
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage>{
  final TextStyle _estiloTexto = new TextStyle(fontSize: 30);
int _cont = 0;
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Título"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            
            Text("Número de Taps: ", style: _estiloTexto),
            Text("$_cont", style: _estiloTexto),
          ],
        ),
      ),
      floatingActionButton: _crearBotones(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        
    );
  }

  Widget _crearBotones(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        SizedBox(width: 30,),
FloatingActionButton(child: Icon(Icons.exposure_zero), onPressed: _igualarA0),
Expanded(child: SizedBox()),
FloatingActionButton(child: Icon(Icons.remove), onPressed: _restar),
Expanded(child: SizedBox()),
FloatingActionButton(child: Icon(Icons.add), onPressed: _agregar,)

      ],
    );
  }

  void _agregar() {
    setState(() => _cont++);
  }
  void _restar() {
    setState(() => _cont--);
  }
  void _igualarA0() {
    setState(() => _cont=0);
  }
}